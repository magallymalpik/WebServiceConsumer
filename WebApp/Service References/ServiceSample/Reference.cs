﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.34014
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebApp.ServiceSample {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Persona", Namespace="http://schemas.datacontract.org/2004/07/ServiceSample.Models")]
    [System.SerializableAttribute()]
    public partial class Persona : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string ApellidoField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string CorreoField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int IdField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string NombreField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Apellido {
            get {
                return this.ApellidoField;
            }
            set {
                if ((object.ReferenceEquals(this.ApellidoField, value) != true)) {
                    this.ApellidoField = value;
                    this.RaisePropertyChanged("Apellido");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Correo {
            get {
                return this.CorreoField;
            }
            set {
                if ((object.ReferenceEquals(this.CorreoField, value) != true)) {
                    this.CorreoField = value;
                    this.RaisePropertyChanged("Correo");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int Id {
            get {
                return this.IdField;
            }
            set {
                if ((this.IdField.Equals(value) != true)) {
                    this.IdField = value;
                    this.RaisePropertyChanged("Id");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Nombre {
            get {
                return this.NombreField;
            }
            set {
                if ((object.ReferenceEquals(this.NombreField, value) != true)) {
                    this.NombreField = value;
                    this.RaisePropertyChanged("Nombre");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="ServiceSample.IPersonaService")]
    public interface IPersonaService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IPersonaService/QueryPersona", ReplyAction="http://tempuri.org/IPersonaService/QueryPersonaResponse")]
        WebApp.ServiceSample.Persona[] QueryPersona();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IPersonaService/QueryPersona", ReplyAction="http://tempuri.org/IPersonaService/QueryPersonaResponse")]
        System.Threading.Tasks.Task<WebApp.ServiceSample.Persona[]> QueryPersonaAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IPersonaService/InsertPersona", ReplyAction="http://tempuri.org/IPersonaService/InsertPersonaResponse")]
        WebApp.ServiceSample.Persona InsertPersona(WebApp.ServiceSample.Persona model);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IPersonaService/InsertPersona", ReplyAction="http://tempuri.org/IPersonaService/InsertPersonaResponse")]
        System.Threading.Tasks.Task<WebApp.ServiceSample.Persona> InsertPersonaAsync(WebApp.ServiceSample.Persona model);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IPersonaService/UpdatePersona", ReplyAction="http://tempuri.org/IPersonaService/UpdatePersonaResponse")]
        WebApp.ServiceSample.Persona UpdatePersona(int id, WebApp.ServiceSample.Persona model);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IPersonaService/UpdatePersona", ReplyAction="http://tempuri.org/IPersonaService/UpdatePersonaResponse")]
        System.Threading.Tasks.Task<WebApp.ServiceSample.Persona> UpdatePersonaAsync(int id, WebApp.ServiceSample.Persona model);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IPersonaService/RemovePersona", ReplyAction="http://tempuri.org/IPersonaService/RemovePersonaResponse")]
        void RemovePersona(int id);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IPersonaService/RemovePersona", ReplyAction="http://tempuri.org/IPersonaService/RemovePersonaResponse")]
        System.Threading.Tasks.Task RemovePersonaAsync(int id);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IPersonaServiceChannel : WebApp.ServiceSample.IPersonaService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class PersonaServiceClient : System.ServiceModel.ClientBase<WebApp.ServiceSample.IPersonaService>, WebApp.ServiceSample.IPersonaService {
        
        public PersonaServiceClient() {
        }
        
        public PersonaServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public PersonaServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public PersonaServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public PersonaServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public WebApp.ServiceSample.Persona[] QueryPersona() {
            return base.Channel.QueryPersona();
        }
        
        public System.Threading.Tasks.Task<WebApp.ServiceSample.Persona[]> QueryPersonaAsync() {
            return base.Channel.QueryPersonaAsync();
        }
        
        public WebApp.ServiceSample.Persona InsertPersona(WebApp.ServiceSample.Persona model) {
            return base.Channel.InsertPersona(model);
        }
        
        public System.Threading.Tasks.Task<WebApp.ServiceSample.Persona> InsertPersonaAsync(WebApp.ServiceSample.Persona model) {
            return base.Channel.InsertPersonaAsync(model);
        }
        
        public WebApp.ServiceSample.Persona UpdatePersona(int id, WebApp.ServiceSample.Persona model) {
            return base.Channel.UpdatePersona(id, model);
        }
        
        public System.Threading.Tasks.Task<WebApp.ServiceSample.Persona> UpdatePersonaAsync(int id, WebApp.ServiceSample.Persona model) {
            return base.Channel.UpdatePersonaAsync(id, model);
        }
        
        public void RemovePersona(int id) {
            base.Channel.RemovePersona(id);
        }
        
        public System.Threading.Tasks.Task RemovePersonaAsync(int id) {
            return base.Channel.RemovePersonaAsync(id);
        }
    }
}
