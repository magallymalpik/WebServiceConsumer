﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApp.Controllers
{
    public class PersonaController : Controller
    {
        //
        // GET: /Persona/
        public ActionResult Index()
        {
            ServiceSample.PersonaServiceClient sv = new ServiceSample.PersonaServiceClient();
            ServiceSample.Persona[] list = sv.QueryPersona();
            return View(list.AsEnumerable());
        }

        //
        // GET: /Persona/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Persona/Create
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Persona/Create
        [HttpPost]
        public ActionResult Create(ServiceSample.Persona model)
        {
            try
            {
                // TODO: Add insert logic here
                ServiceSample.PersonaServiceClient sv = new ServiceSample.PersonaServiceClient();
                sv.InsertPersona(model);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Persona/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Persona/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, ServiceSample.Persona model)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Persona/Delete/5
        public ActionResult Delete(int id)
        {
            ServiceSample.PersonaServiceClient sv = new ServiceSample.PersonaServiceClient();
            sv.RemovePersona(id);
            return RedirectToAction("Index");
        }

        //
        // POST: /Persona/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
