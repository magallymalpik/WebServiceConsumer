﻿using ServiceSample.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ServiceSample
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "PersonaService" en el código, en svc y en el archivo de configuración a la vez.
    // NOTA: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione PersonaService.svc o PersonaService.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class PersonaService : IPersonaService
    {
        public List<Models.Persona> QueryPersona()
        {
            var list = new List<Models.Persona>();
            using (var db = new ModelContainer1())
            {
                list = db.Persona.ToList();
            }

            return list;
        }

        public Models.Persona InsertPersona(Models.Persona model)
        {
            using (var db = new ModelContainer1())
            {
                db.Persona.Add(model);

                if (db.ChangeTracker.HasChanges())
                {
                    db.SaveChanges();
                }
            }

            return model;
        }

        public Persona UpdatePersona(int id, Persona model)
        {
            throw new NotImplementedException();
        }

        public void RemovePersona(int id)
        {
            using (var db = new ModelContainer1())
            {
                var model = db.Persona.Single(x => x.Id == id);
                db.Persona.Remove(model);

                if (db.ChangeTracker.HasChanges())
                {
                    db.SaveChanges();
                }
            }
        }
    }
}
