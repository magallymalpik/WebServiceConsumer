﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ServiceSample
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IPersonaService" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IPersonaService
    {
        [OperationContract]
        List<Models.Persona> QueryPersona();

        [OperationContract]
        Models.Persona InsertPersona(Models.Persona model);

        [OperationContract]
        Models.Persona UpdatePersona(int id, Models.Persona model);

        [OperationContract]
        void RemovePersona(int id);
    }
}
